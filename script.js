// console.log('created');
const checkbox = document.getElementById('slider-input');
const planRate = document.getElementsByClassName('plan-rate')

function changePlan() {
    // console.log('working', checkbox.checked);
    if (checkbox.checked) {
        // console.log('monthly', );
        planRate[0].innerHTML = '<span>$</span> 19.99'
        planRate[1].innerHTML = '<span>$</span> 24.99'
        planRate[2].innerHTML = '<span>$</span> 39.99'
    } else {
        // console.log('annual', )
        planRate[0].innerHTML = '<span>$</span> 199.99'
        planRate[1].innerHTML = '<span>$</span> 249.99'
        planRate[2].innerHTML = '<span>$</span> 399.99'
    }
}

changePlan();